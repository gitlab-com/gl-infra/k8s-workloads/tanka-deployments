local k = (import 'ksonnet-util/kausal.libsonnet') + {
  _config+:: {
    namespace: 'evicted-pod-reaper',
  },
};

local cronjob = k.batch.v1.cronJob;
local container = k.core.v1.container;

local _config = {
  image: {
    repository: 'bitnami/kubectl',
    // renovate: datasource=docker depName=bitnami/kubectl versioning=docker
    tag: '1.32',
  },
  command: ['/bin/sh', '-c', 'kubectl -n gitlab get pods -o jsonpath=\'{.items[?(@.status.reason=="Evicted")].metadata.name}\' | xargs -r kubectl -n gitlab delete pod'],
  rbacRules: [
    {
      apiGroups: [''],
      resources: ['pods'],
      verbs: ['get', 'list', 'delete'],
    },
  ],
};

{
  newCronJob(name='evicted-pod-reaper', schedule='*/30 * * * *'):: {
    cronJob: cronjob.new(
      name=name,
      schedule=schedule,
      containers=[container.new(
        name=name,
        image=std.join(':', [_config.image.repository, _config.image.tag]),
      ) + {
        command: _config.command,
      }],
    ) + {
      spec+: {
        jobTemplate+: {
          spec+: {
            template+: {
              spec+: {
                restartPolicy: 'Never',
                serviceAccountName: name,
              },
            },
          },
        },
      },
    },

    rbac: k.util.rbac(name=name, rules=_config.rbacRules),
  },
}
