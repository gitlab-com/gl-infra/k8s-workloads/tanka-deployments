local k = import 'k.libsonnet';

local configMap = k.core.v1.configMap,
      container = k.core.v1.container,
      containerPort = k.core.v1.containerPort,
      envVar = k.core.v1.envVar,
      weightedPodAffinityTerm = k.core.v1.weightedPodAffinityTerm,
      podDisruptionBudget = k.policy.v1.podDisruptionBudget,
      podTemplate = statefulSet.spec.template,
      pvcTemplate = k.core.v1.persistentVolumeClaimTemplate,
      securityContext = podTemplate.spec.securityContext,
      service = k.core.v1.service,
      serviceAccount = k.core.v1.serviceAccount,
      servicePort = k.core.v1.servicePort,
      statefulSet = k.apps.v1.statefulSet,
      volume = k.core.v1.volume,
      volumeMount = k.core.v1.volumeMount;

{
  local defaults = {
    image: {
      repository: 'registry.ops.gitlab.net/gitlab-com/gl-infra/fluentd-docker',
      pullPolicy: 'IfNotPresent',
      tag: 'v7.0.0',
    },
    imagePullSecrets: [],
    resources: {
      requests: {
        cpu: '400m',
        memory: '1500Mi',
      },
      limits: {
        memory: '2.5Gi',
      },
    },
    replicas: 1,
    runAs: {
      gid: 999,
      uid: 999,
    },
    gcs: {
      path: 'gke/${tag}/dt=%Y-%m-%d/',
      flushInterval: '5m',
      storeAs: 'gzip',
    },
    pubsub: {
      max_messages: 500,
      pull_interval: 5.0,
      pull_threads: 4,
      topics: [],
    },
    metrics: {
      port: 24231,
    },
    workers: 1,
    googleServiceAccount: null,
  },

  new(env, namespace, name='fluentd-archiver', config={})::
    local _config = defaults + config;

    local fluentConf = |||
      # do not collect fluentd logs to avoid infinite loops.
      <label @FLUENT_LOG>
        <match **>
          @type null
          @id ignore_fluent_logs
        </match>
      </label>

      @include config.d/*.conf
    |||;
    local workersConfTpl = |||
      <system>
        workers %(workers)s
      </system>
    |||;
    local prometheusConfTpl = |||
      <source>
        @type prometheus
        @id in_prometheus
        bind "0.0.0.0"
        port %(metricsPort)s
        metrics_path "/metrics"
      </source>

      <source>
        @type prometheus_monitor
        @id in_prometheus_monitor
      </source>

      <source>
        @type prometheus_output_monitor
        @id in_prometheus_output_monitor
      </source>
    |||;
    local pubsubSourceConfTpl = |||
      <source>
        @type gcloud_pubsub
        topic pubsub-%(topic)s-inf-%(env)s
        subscription pubsub-%(topic)s-inf-%(env)s-fluentd-archiver
        max_messages %(max_messages)s
        pull_interval %(pull_interval)s
        pull_threads %(pull_threads)s
        tag %(topic)s
        <parse>
          @type json
          keep_time_key true
          time_key time
          time_format %%iso8601
          time_format_fallbacks %%Y-%%m-%%d %%H:%%M:%%S %%z, unixtime
          time_type mixed
        </parse>
      </source>
    |||;
    local gcsOutputConfTpl = |||
      <match **>
        @type gcs

        bucket gitlab-%(env)s-logging-archive
        object_key_format %%{path}%%{time_slice}_%%{index}_%%{uuid_flush}.%%{file_extension}
        path %(path)s
        store_as %(storeAs)s

        <buffer tag,time>
          @type file
          path /fluentd/log/gcs
          flush_at_shutdown true
          timekey %(flushInterval)s
          timekey_wait 0
          timekey_use_utc true
        </buffer>

        <format>
          @type json
        </format>
      </match>
    |||;


    local
      workersConf = std.format(workersConfTpl, {
        workers: _config.workers,
      }),
      prometheusConf = std.format(prometheusConfTpl, {
        metricsPort: _config.metrics.port,
      }),
      sourcesConf = std.join('\n', [
        std.format(pubsubSourceConfTpl, {
          env: _config.name,
          max_messages: _config.pubsub.max_messages,
          pull_interval: _config.pubsub.pull_interval,
          pull_threads: _config.pubsub.pull_threads,
          topic: topic,
        })
        for topic in _config.pubsub.topics
      ]),
      gcsOutputsConf = std.format(gcsOutputConfTpl, {
        env: _config.name,
        path: _config.gcs.path,
        flushInterval: _config.gcs.flushInterval,
        storeAs: _config.gcs.storeAs,
      });

    local selector_labels = {
      'app.kubernetes.io/instance': name,
      'app.kubernetes.io/name': name,
    };

    local labels = selector_labels;

    local deployment_labels = labels {
      type: 'logging',
      deployment: name,
      [if 'stageLabel' in _config then 'stage']: _config.stageLabel,
    };

    local pod_annotations = {
      'fluent-conf-hash': std.md5(fluentConf),
      'workers-conf-hash': std.md5(workersConf),
      'prometheus-conf-hash': std.md5(prometheusConf),
      'sources-conf-hash': std.md5(sourcesConf),
      'gcp-outputs-conf-hash': std.md5(gcsOutputsConf),
    };

    local service_monitor_relabelings = [
      { targetLabel: 'type', replacement: 'logging' },
      { targetLabel: 'tier', replacement: 'inf' },
      { targetLabel: 'stage', replacement: 'main' },
      { targetLabel: 'shard', replacement: 'default' },
      { targetLabel: 'environment', replacement: env },
    ];

    {
      serviceAccount:
        serviceAccount.new(name)
        + serviceAccount.metadata.withAnnotations({
          'iam.gke.io/gcp-service-account': _config.googleServiceAccount,
        })
        + serviceAccount.metadata.withLabels(labels),

      configMaps: {
        main:
          configMap.new(name + '-fluentd-main')
          + configMap.metadata.withLabels(labels)
          + configMap.withData(
            {
              'fluent.conf': fluentConf,
            }
          ),
        config:
          configMap.new(name + '-fluentd-config')
          + configMap.metadata.withLabels(labels)
          + configMap.withData({
            '00-workers.conf': workersConf,
            '01-prometheus.conf': prometheusConf,
            '02-sources.conf': sourcesConf,
            '03-outputs.conf': gcsOutputsConf,
          }),
      },

      service:
        service.new(
          name + '-metrics',
          selector=selector_labels,
          ports=[
            servicePort.newNamed('metrics', _config.metrics.port, 'metrics')
            + servicePort.withProtocol('TCP'),
          ],
        )
        + service.metadata.withLabels(labels)
        + service.spec.withType('ClusterIP'),

      service_monitor: {
        apiVersion: 'monitoring.coreos.com/v1',
        kind: 'ServiceMonitor',
        metadata: {
          name: name,
          labels: labels {
            'gitlab.com/prometheus-instance': 'prometheus-system',
          },
        },
        spec: {
          endpoints: [
            {
              interval: '15s',
              scrapeTimeout: '5s',
              honorLabels: true,
              port: 'metrics',
              path: '/metrics',
              scheme: 'http',
              relabelings: service_monitor_relabelings,
            },
          ],
          jobLabel: name,
          selector: {
            matchLabels: selector_labels,
          },
          namespaceSelector: {
            matchNames: [namespace],
          },
        },
      },

      sts:
        statefulSet.new(
          name,
          replicas=_config.replicas,
          podLabels=deployment_labels,
          containers=[
            container.new('fluentd', std.join(':', [_config.image.repository, _config.image.tag]))
            + container.withImagePullPolicy(_config.image.pullPolicy)
            + container.withEnv([
              envVar.new('FLUENTD_CONF', '../../etc/fluent/fluent.conf'),
              // Default is 2.0, lowering it to reduce memory usage, see https://docs.fluentd.org/deployment/performance-tuning-single-process#reduce-memory-usage.
              envVar.new('RUBY_GC_HEAP_OLDOBJECT_LIMIT_FACTOR', '1.2'),
            ])
            + container.withPorts([
              containerPort.newNamed(_config.metrics.port, 'metrics')
              + containerPort.withProtocol('TCP'),
            ])
            + container.livenessProbe.httpGet.withPort('metrics')
            + container.livenessProbe.httpGet.withPath('/metrics')
            + container.readinessProbe.httpGet.withPort('metrics')
            + container.readinessProbe.httpGet.withPath('/metrics')
            + container.withVolumeMounts([
              volumeMount.new('etcfluentd-main', '/etc/fluent', true),
              volumeMount.new('etcfluentd-config', '/etc/fluent/config.d', true),
              volumeMount.new('fluentd-gcs-log-buffer', '/fluentd/log/gcs'),
            ])
            + container.resources.withRequests(_config.resources.requests)
            + container.resources.withLimits(_config.resources.limits),
          ],
        )
        + securityContext.withFsGroup(_config.runAs.gid)
        + securityContext.withRunAsGroup(_config.runAs.gid)
        + securityContext.withRunAsUser(_config.runAs.uid)
        + statefulSet.metadata.withLabels(deployment_labels)
        + statefulSet.spec.withServiceName(name + '-metrics')
        + statefulSet.spec.withVolumeClaimTemplates([
          pvcTemplate.metadata.withName('fluentd-gcs-log-buffer')
          + pvcTemplate.spec.withAccessModes('ReadWriteOnce')
          + pvcTemplate.spec.withStorageClassName('ssd')
          + pvcTemplate.spec.resources.withRequests({ storage: '2Gi' }),
        ])
        + statefulSet.spec.withPodManagementPolicy('Parallel')
        + podTemplate.metadata.withAnnotations(pod_annotations)
        + podTemplate.spec.withImagePullSecrets([{ name: secret } for secret in _config.imagePullSecrets])
        + podTemplate.spec.withServiceAccountName(name)
        + podTemplate.spec.withVolumes([
          volume.fromConfigMap('etcfluentd-main', name + '-fluentd-main'),
          volume.fromConfigMap('etcfluentd-config', name + '-fluentd-config'),
        ])
        + podTemplate.spec.affinity.podAntiAffinity.withPreferredDuringSchedulingIgnoredDuringExecution([
          weightedPodAffinityTerm.withWeight(1)
          + weightedPodAffinityTerm.podAffinityTerm.labelSelector.withMatchLabels(selector_labels)
          + weightedPodAffinityTerm.podAffinityTerm.withTopologyKey('kubernetes.io/hostname'),
        ]),

      pdb:
        podDisruptionBudget.new(name)
        + podDisruptionBudget.metadata.withLabels(labels)
        + podDisruptionBudget.spec.selector.withMatchLabels(selector_labels)
        + podDisruptionBudget.spec.withMaxUnavailable('10%'),
    },
}
