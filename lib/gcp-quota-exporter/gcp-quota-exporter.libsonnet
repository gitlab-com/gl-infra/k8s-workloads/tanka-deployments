local common = import 'gitlab/common.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';

local container = k.core.v1.container,
      port = k.core.v1.containerPort,
      serviceAccount = k.core.v1.serviceAccount;

local probe(port) = {
  httpGet: {
    path: '/',
    port: port,
  },
  initialDelaySeconds: 60,
  timeoutSeconds: 10,
};

{
  configDefaults:: {
    image: 'registry.gitlab.com/gitlab-com/gl-infra/gcp-quota-exporter',
    tag: 'v0.5.0',
    replicas: 1,
    resources: {},
    svcPort: 9592,
    googleProject: 'my-project',
    scrapeProjects: 'my-project',
    scrapeRegions: std.join('\n', [
      'us-east1',
      'us-east4',
      'us-central1',
    ]),
    googleServiceAccount: 'gcp-quota-exporter@%s.iam.gserviceaccount.com' % self.googleProject,
    serviceAccountName: 'gcp-quota-exporter',
    createServiceAccount: true,
  },

  newExporter(env, appName, namespace, config)::
    local envConfig = $.configDefaults + config;
    {
      deployment: $.deployment(env, appName, config),
      service: $.serviceFor(self.deployment),
      serviceAccount: if envConfig.createServiceAccount then $.serviceAccount(appName, config),
      serviceMonitor: $.serviceMonitor(env, appName, config),
    },

  deployment(env, appName, config)::
    local envConfig = $.configDefaults + config;
    common.deployment(
      name=appName,
      type='monitoring',
      replicas=envConfig.replicas,
      containers=[
        container.new(
          name='gcp-quota-exporter',
          image='%s:%s' % [envConfig.image, envConfig.tag],
        )
        + container.withPorts([port.new('http', envConfig.svcPort)])
        + container.withEnvMap({
          GOOGLE_PROJECT_ID: envConfig.scrapeProjects,
          GOOGLE_REGION: envConfig.scrapeRegions,
          GCP_EXPORTER_MAX_RETRIES: '10',
          GCP_EXPORTER_HTTP_TIMEOUT: '2m',
        })
        + {
          // Take imagePullPolicy private to remove it from the final object.
          // We want to rely on the default behaviour of imagePullPolicy to
          // avoid using stale `:latest` images.
          imagePullPolicy:: super.imagePullPolicy,
          livenessProbe: probe(envConfig.svcPort),
          readinessProbe: probe(envConfig.svcPort),
          resources: envConfig.resources,
        },
      ],
    ) + {
      metadata+: {
        labels+: {
          name: appName,
        },
      },
      spec+: {
        template+: {
          spec+: {
            serviceAccountName: envConfig.serviceAccountName,
          },
        },
      },
    },

  serviceFor(deployment)::
    k.util.serviceFor(deployment) + {
      metadata+: {
        annotations+: {
          'cloud.google.com/neg': '{"ingress": true}',
        },
      },
      spec+: {
        type: 'NodePort',
      },
    },

  serviceAccount(appName, config):
    local envConfig = $.configDefaults + config;

    serviceAccount.new(envConfig.serviceAccountName)
    + serviceAccount.metadata.withAnnotations({
      'iam.gke.io/gcp-service-account': envConfig.googleServiceAccount,
    })
    + serviceAccount.metadata.withLabels({
      'app.kubernetes.io/instance': appName,
      'app.kubernetes.io/name': 'gcp-quota-exporter',
    }),

  serviceMonitor(env, appName, config):: {
    local envConfig = $.configDefaults + config,

    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'ServiceMonitor',
    metadata: {
      name: appName,
      labels: {
        'app.kubernetes.io/component': 'metrics',
        'app.kubernetes.io/instance': appName,
        'app.kubernetes.io/name': 'gcp-quota-exporter',
        'gitlab.com/prometheus-instance': 'prometheus-system',
      },
    },
    spec: {
      endpoints: [
        {
          interval: '5m',
          scrapeTimeout: '4m',
          path: '/metrics',
          targetPort: envConfig.svcPort,
          relabelings: [
            {
              targetLabel: 'type',
              replacement: 'monitoring',
            },
            {
              targetLabel: 'tier',
              replacement: 'inf',
            },
            {
              targetLabel: 'stage',
              replacement: 'main',
            },
            {
              targetLabel: 'shard',
              replacement: 'default',
            },
            {
              targetLabel: 'environment',
              replacement: env,
            },
          ],
          metricRelabelings: [
            {
              sourceLabels: ['region'],
              targetLabel: 'quotaregion',
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          name: appName,
        },
      },
    },
  },
}
