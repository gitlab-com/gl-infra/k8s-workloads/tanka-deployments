local vaultServers = {
  ops: 'https://vault.ops.gke.gitlab.net',
  pre: 'https://vault.pre.gke.gitlab.net',
};

{
  gprd: {
    contextNames: [
      'gke_gitlab-production_us-east1_gprd-gitlab-gke',
      'production.teleport.gitlab.net-gprd-gitlab-gke',
    ],
    googleProject: 'gitlab-production',
    clusterName: 'gprd-gitlab-gke',
    name: 'gprd',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  'gprd-us-east1-b': $.gprd {
    contextNames: [
      'gke_gitlab-production_us-east1-b_gprd-us-east1-b',
      'production.teleport.gitlab.net-gprd-us-east1-b',
    ],
    clusterName: 'gprd-us-east1-b',
    name: 'gprd-us-east1-b',
    type: 'zonal',
  },
  'gprd-us-east1-c': $.gprd {
    contextNames: [
      'gke_gitlab-production_us-east1-c_gprd-us-east1-c',
      'production.teleport.gitlab.net-gprd-us-east1-c',
    ],
    clusterName: 'gprd-us-east1-c',
    name: 'gprd-us-east1-c',
    type: 'zonal',
  },
  'gprd-us-east1-d': $.gprd {
    contextNames: [
      'gke_gitlab-production_us-east1-d_gprd-us-east1-d',
      'production.teleport.gitlab.net-gprd-us-east1-d',
    ],
    clusterName: 'gprd-us-east1-d',
    name: 'gprd-us-east1-d',
    type: 'zonal',
  },
  'gprd-gitlab-3okls': {
    contextNames: [
      'gke_gitlab-production_us-east1_gitlab-3okls',
      'production.teleport.gitlab.net-gitlab-3okls',
    ],
    googleProject: 'gitlab-production',
    region: 'us-east1',
    cluster: 'gitlab-3okls',
    clusterName: 'gprd-gitlab-3okls',
    name: 'gprd-gitlab-3okls',
    type: 'regional',
    legacyDNS: false,
    vaultServer: vaultServers.ops,
  },
  gstg: {
    contextNames: [
      'gke_gitlab-staging-1_us-east1_gstg-gitlab-gke',
      'staging.teleport.gitlab.net-gstg-gitlab-gke',
    ],
    googleProject: 'gitlab-staging-1',
    clusterName: 'gstg-gitlab-gke',
    name: 'gstg',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  'gstg-us-east1-b': $.gstg {
    contextNames: [
      'gke_gitlab-staging-1_us-east1-b_gstg-us-east1-b',
      'staging.teleport.gitlab.net-gstg-us-east1-b',
    ],
    clusterName: 'gstg-us-east1-b',
    name: 'gstg-us-east1-b',
    type: 'zonal',
  },
  'gstg-us-east1-c': $.gstg {
    contextNames: [
      'gke_gitlab-staging-1_us-east1-c_gstg-us-east1-c',
      'staging.teleport.gitlab.net-gstg-us-east1-c',
    ],
    clusterName: 'gstg-us-east1-c',
    name: 'gstg-us-east1-c',
    type: 'zonal',
  },
  'gstg-us-east1-d': $.gstg {
    contextNames: [
      'gke_gitlab-staging-1_us-east1-d_gstg-us-east1-d',
      'staging.teleport.gitlab.net-gstg-us-east1-d',
    ],
    clusterName: 'gstg-us-east1-d',
    name: 'gstg-us-east1-d',
    type: 'zonal',
  },
  'gstg-gitlab-36dv2': {
    contextNames: [
      'gke_gitlab-staging-1_us-east1_gitlab-36dv2',
      'staging.teleport.gitlab.net-gitlab-36dv2',
    ],
    googleProject: 'gitlab-staging-1',
    region: 'us-east1',
    cluster: 'gitlab-36dv2',
    clusterName: 'gstg-gitlab-36dv2',
    name: 'gstg-gitlab-36dv2',
    type: 'regional',
    legacyDNS: false,
    vaultServer: vaultServers.ops,
  },
  ops: {
    contextNames: [
      'gke_gitlab-ops_us-east1_ops-gitlab-gke',
      'production.teleport.gitlab.net-ops-gitlab-gke',
    ],
    googleProject: 'gitlab-ops',
    clusterName: 'ops-gitlab-gke',
    name: 'ops',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  'ops-central': {
    contextNames: [
      'gke_gitlab-ops_us-central1_ops-central',
      'production.teleport.gitlab.net-ops-central',
    ],
    googleProject: 'gitlab-ops',
    clusterName: 'ops-central',
    name: 'ops-central',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  pre: {
    contextNames: [
      'gke_gitlab-pre_us-east1_pre-gitlab-gke',
      'staging.teleport.gitlab.net-pre-gitlab-gke',
    ],
    googleProject: 'gitlab-pre',
    clusterName: 'pre-gitlab-gke',
    name: 'pre',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  prdsub: {
    contextNames: [
      'gke_gitlab-subscriptions-prod_us-east1_prdsub-customers-gke',
      'production.teleport.gitlab.net-prdsub-customers-gke',
    ],
    googleProject: 'gitlab-subscriptions-prod',
    clusterName: 'prdsub-customers-gke',
    name: 'prdsub',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  stgsub: {
    contextNames: [
      'gke_gitlab-subscriptions-staging_us-east1_stgsub-customers-gke',
      'staging.teleport.gitlab.net-stgsub-customers-gke',
    ],
    googleProject: 'gitlab-subscriptions-staging',
    clusterName: 'stgsub-customers-gke',
    name: 'stgsub',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  'stgsub-ref': {
    contextNames: [
      'gke_gitlab-subscriptions-stg-ref_us-east1_stgsub-ref-customers-gke',
      'staging.teleport.gitlab.net-stgsub-ref-customers-gke',
    ],
    googleProject: 'gitlab-subscriptions-stg-ref',
    clusterName: 'stgsub-ref-customers-gke',
    name: 'stgsub-ref',
    type: 'regional',
    vaultServer: vaultServers.ops,
  },
  dev: {
    contextNames: [
      'minikube',
    ],
    googleProject: 'dummy',
    clusterName: 'dev',
    name: 'dev',
    type: 'dev',
    vaultServer: vaultServers.pre,
  },
}
