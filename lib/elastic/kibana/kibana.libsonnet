local tanka = import 'github.com/grafana/jsonnet-libs/tanka-util/main.libsonnet';
local helm = tanka.helm.new(std.thisFile);
{
  defaultKibana(name='kibana', chartPath='../../../charts/kibana', conf={}):: {
    kibana: helm.template(name, chartPath, conf),
  },
}
