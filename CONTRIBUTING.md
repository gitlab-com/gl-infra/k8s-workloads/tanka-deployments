
[[_TOC_]]

# Getting Started #

## Install tanka-deployments dependencies ##

`tanka-deployments` depends on tools and libraries managed using three mechanisms:
- `asdf`
- `jsonnet-bundler`
- `tk` (tanka itself)

### asdf ###

Installation steps:
1. Follow asdf documentation for installation: https://asdf-vm.com/guide/getting-started.html
1. Install tools managed with asdf:
```
$ cd tanka-deployments
$ asdf install  # uses `.tool-versions` as the source of truth
```

### jsonnet-bundler and tk (tanka) ###

This repository contains non-checked in dependencies that are managed by package
managers such as jsonnet-bundler. Run `make vendor` to ensure that your local
dependencies are up-to-date before running `tk` commands against this
repository.

## Run diff against a local dev kubernetes cluster ##

k3d:
```
$ k3d cluster create --api-port 6443 -p '127.0.0.1:16443:6443@server[0]' --no-lb \
  && kubectl config set-cluster k3d-k3s-default --server=https://127.0.0.1:16443
$ cd tanka-deployments
$ tk diff environments/evicted-pod-reaper --name evicted-pod-reaper/dev
```

minikube (for more details see [docs below](#the-dev-environment)):
```
$ minikube start --memory 20480 --cpus 5 --driver=virtualbox  # create minikube
$ vboxmanage controlvm $(vboxmanage list vms | grep minikube | awk {'print $1'} | sed 's/"//g') natpf1 "apiserver,tcp,127.0.0.1,16443,,8443"  # add port-forwarding to the VirtualBox VM network interface config
$ kubectl config set-cluster minikube --server=https://127.0.0.1:16443  # update `~/.kube/config`
$ cd tanka-deployments
$ tk diff environments/evicted-pod-reaper --name evicted-pod-reaper/dev
```

## Run diff against one of the deployed environments ##

If you haven't yet, please follow this document: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md to obtain access to our k8s infrastructure. You need to have credentials configured in your `~/.kube/config` for tanka to work.

```
$ cd tanka-deployments
$ tk diff environments/evicted-pod-reaper --name evicted-pod-reaper/gprd
```

## Recommended readings ##

It is strongly recommended that you familiarize yourself with the basics of jsonnet and tanka from their docs before tackling the rest of this document.

1. jsonnet tutorial: https://jsonnet.org/learning/tutorial.html
1. tanka tutorial: https://tanka.dev/tutorial/overview
1. tanka inline environments documentation: https://tanka.dev/inline-environments

# tanka-deployments project #

## Repository structure ##

### Root directory ###

The top-level directory mainly consists of a single tanka structure created with
`tk init`.

### environments ###

There are 2 flavors of tanka environment: static and [inline](https://tanka.dev/inline-environments). Inline environments are strongly encouraged. If you see static environments in this repository, chances are they haven't been migrated yet to inline.

In tanka, an Environment is some logical group of pieces that form an application stack deployed to a specific kubernetes cluster or a namespace. For more details see: https://tanka.dev/tutorial/jsonnet#environments . For example, for `evicted-pod-reaper` we have the following environments: `environments/evicted-pod-reaper/dev` and `environments/evicted-pod-reaper/gprd`

The entry point for many tanka commands is the `main.jsonnet` file in the environment given as the argument. Tanka commands evaluate jsonnet code from that file to a json string consisting of environment objects definitions. For example, when running `tk eval environments/evicted-pod-reaper` you are telling tanka to evaluate `./environments/evicted-pod-reaper/main.jsonnet` file. The resulting json will look something like:
```json

{
  "envs": {
    "evicted-pod-reaper/dev": {
      "apiVersion": "tanka.dev/v1alpha1",
      "data": {
      (...)
      },
      "kind": "Environment",
      "metadata": {
        "name": "environments/evicted-pod-reaper/dev"
      },
      "spec": {
        "apiServer": "https://127.0.0.1:16443",
        "expectVersions": {},
        "injectLabels": true,
        "namespace": "evicted-pod-reaper",
        "resourceDefaults": {}
      }
    },
    "evicted-pod-reaper/gprd": {
      "apiVersion": "tanka.dev/v1alpha1",
      "data": {
      (...)
      },
      "kind": "Environment",
      "metadata": {
        "name": "environments/evicted-pod-reaper/gprd"
      },
      "spec": {
        "apiServer": "https://35.243.230.38",
        "expectVersions": {},
        "injectLabels": true,
        "namespace": "evicted-pod-reaper",
        "resourceDefaults": {}
      }
    }
  }
}
```

Using the `--name` parameter you can tell `tk diff`, `tk apply`, and `tk show`, which environment to use. For example: `tk diff environments/evicted-pod-reaper --name evicted-pod-reaper/gprd`

`main.jsonnet` should only contain environments definitions, i.e. mapping between values specific to an environment and libraries containing k8s object definitions for a given application. See `./environments/evicted-pod-reaper/main.jsonnet` for an example. The diagram below illustrates how different pieces of config are separated using jsonnet abstractions.

![Abstractions](./img/abstractions.png)

Values (literals) for simple environments can be contained within `main.jsonnet`, but for more complex ones it sometimes makes sense to move it to a dedicated file.

### lib ###

If there is no jsonnet-bundler managed release for a piece of software, we write the relevant jsonnet code ourselves and put it in this directory. This is where majority of your code should live. Think of the `lib` directory as a location for publishing tanka code to the world. We just happen to be using it ourselves. This is an equivalent of: https://github.com/grafana/jsonnet-libs

### vendor ###

External libraries that were "vendored into" this repository using jsonnet-bundler. Vendoring is a concept from Golang: https://stackoverflow.com/questions/26217488/what-is-vendoring . Conceptually it's the opposite of using a dependency manager, i.e. it's about storing a copy of the source code of a third party library/app directly in your repository.

### charts ###

Helm charts used by tanka's helm integration. For more details see
[How to add a new chart](#adding-a-new-chart)

The charts are vendored as well. If you make a local override to them
while waiting for an upstream change to get merged. Add an
`ignore_ensure_vendored` file to the root of the chart's
directory. The contents of the file should contain the link to an
issue or merge request for updating the dependency.

## Inter-deployment dependencies ##

Some tanka deployments may have external dependencies, referenced in
configuration or secrets. It's possible to develop a tanka deployment that
depends on another tanka deployment. There is no particular orchestration or
management of inter-deployments dependencies in this repository, and "apply"
jobs for different deployments run in parallel.

If you are creating a new deployment that cannot sensibly be deployed without a
dependency already in place, your merge request should only be merged once that
dependency is in place, whether the dependency is another tanka deployment or
not.

## CI/CD ##

### Unattended deployments

There is no manual gating in the CI/CD pipeline. Once a change is merged to master it will be applied to all relevant environments.

### Pipelines on ops.gitlab.net

This repository is public, but our pipelines run on a private mirror on
ops.gitlab.net. The main reason for this is in order to keep our deployment logs
private.

We run
[Woodhouse](https://gitlab.com/gitlab-com/gl-infra/woodhouse#gitlab-notify-mirrored-mr-subcommand)
in these mirror pipelines on ops.gitlab.net to post links to these remote pipelines in relevant
merge requests on gitlab.com .

### Triggering a deployment from another project

It is possible to trigger a pipeline using an API call. This is done by triggering a pipeline on the ops mirror,
passing in `TANKA_DEPLOYMENTS_RUN=<environment name>`. Note that the environment
name here is not the [GitLab
environment](https://docs.gitlab.com/ee/ci/environments/) name, but the tanka
environment name: for example, "woodhouse/ops", or "woodhouse/gstg". You can see an example of this in [woodhouse's deploy
jobs](https://gitlab.com/gitlab-com/gl-infra/woodhouse/-/blob/master/.gitlab-ci.yml) . That repository's deploy job triggers a
tanka-deployments pipeline using the API, passing in
`TANKA_DEPLOYMENTS_RUN=woodhouse-ops`, and also overriding that job's extra env
var `WOODHOUSE_TAG` with the image tag we're deploying, which is injected into
tanka config as an extVar.

With a combination of API-triggered pipelines, `TANKA_DEPLOYMENTS_RUN`,
job-specific environment variables and tanka flags, you can build automatic
unattended deployment pipelines for your application using tanka.

## The dev environment ##

The `dev` environment for each tanka deployment is special, in that its API
server is not a globally routable shared resource. You should use this
environment to test resource configuration against a local Kubernetes
environment, e.g. minikube, k3d, or KinD.

We've chosen `localhost:16443` as the address of all dev environment's API
servers. Tanka doesn't support overriding the API server on the command line yet (see: https://github.com/grafana/tanka/issues/522 ), so
you'll either have to configure your Kubernetes dev environment to use this
port, or change `./lib/gitlab/clusters.libsonnet` locally, and not commit the changed
address.

Here is a non-exhaustive list of Kubernetes development tools, and how to
configure them to use this address for the API server:

### k3d ###

Use a non-random API port, and replace `0.0.0.0` with a loopback address in
kubeconfig, to improve chances of compatibility with other dev environments that
can also make use of port forwarding.

```
k3d cluster create --api-port 6443 -p '127.0.0.1:16443:6443@server[0]' --no-lb \
  && kubectl config set-cluster k3d-k3s-default --server=https://127.0.0.1:16443
```

### minikube ###

Minikube with the virtualbox driver uses the VM's external IP address for binding `kube-api-server` and there's no way to change that. In order to set the VM's external IP address to a static value, multiple changes would be required. A much simpler solution is to do port-forwarding after the cluster was created:
```bash
$ minikube start --memory 20480 --cpus 5 --driver=virtualbox  # create minikube
$ vboxmanage controlvm $(vboxmanage list vms | grep minikube | awk {'print $1'} | sed 's/"//g') natpf1 "apiserver,tcp,127.0.0.1,16443,,8443"  # add port-forwarding to the VirtualBox VM network interface config
$ kubectl config set-cluster minikube --server=https://127.0.0.1:16443  # update `~/.kube/config`
```

`minikube start` will overwrite `~/.kube/config` every time you start the VM, so remember to always update the cluster's URL when you start minikube, i.e.:
```
$ minikube start
$ kubectl config set-cluster minikube --server=https://127.0.0.1:16443  # update `~/.kube/config`
```

#### Install helm dependencies

Some dependencies may not be available on a local setup (e.g. minikube). For setting up `monitoring.coreos.com/v1`, run:

```
# https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack#get-helm-repository-info
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
helm install --generate-name prometheus-community/kube-prometheus-stack
```

#### Seed existing secrets

For local dev deployment, `mountVolume.setup` may fail since there are no secrets available. Create one using:

```
kubectl create secret generic --namespace=<deployment's namespace> <secret-name> --from-literal=<key>=<value>
# e.g. kubectl create secret generic --namespace=redis  redis-registry-cache-password --from-literal=redis-password=redis-password
```


## Design doc ##

1. tanka readiness review: https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/49

# FAQ #

## Development tips

- Configure your editor do run `jsonnetfmt` on file save

## Why does the order of objects differ depending on the `tk` subcommand? ##

`tk apply` sorts objects in order to support installation of resources that
depend on others in the same apply: e.g. CRDs before instances of them,
StorageClasses before PersistentVolumeClaims. `tk show` shows the order that `tk
apply` will pass to `kubectl apply`.

`tk diff` relies on `kubectl` server-side diffs, which do not support objects
that depend on others in the same list of manifests, regardless of order. Here's
the relevant tanka issue: https://github.com/grafana/tanka/issues/246 .

## How to create a new Environment? (deploy a new application) ##

```
$ mkdir ./lib/<your_application>                               # create a jsonnet library which can be imported in environments
$ vim ./lib/<your_application>/<your_application>.libsonnet    # write jsonnet code here
$ mkdir ./environments/<your_application>                      # create environments for your application
$ vim ./environments/<your_application>/main.jsonnet           # populate the main.jsonnet file, you might want to start with a dev environment
$ tk eval environments/<your_application>                      # evaluate your environments
```

Tanka doesn't create kubernetes namespaces for environments out of the box (see: https://tanka.dev/tutorial/jsonnet#connecting-to-the-cluster ). These need to be created and managed explicitly. It is common at Gitlab to deploy applications to dedicated namespaces. For this reason, the code in `./lib/gitlab/common.libsonnet` that generates environments objects can also create namespaces. All you need to do to use it is set a hidden field called `createNamespace` on your environment config. See `./environments/evicted-pod-reaper/main.jsonnet` for an example.

## How to use a helm chart for my environment? ##

Tanka can [template helm charts into jsonnet objects](https://tanka.dev/helm).
This is useful when deploying resources that are only distributed through helm,
and allows us to keep up with lengthy, relatively non-customizable resources
such as CRDs and Roles.

### Adding a new chart ###

1. If the chart's repository does not already exist in `chartfile.yaml`, add it with `tk tool charts add-repo [NAME] [URL]`, for example: `tk tool charts add-repo elastic https://helm.elastic.co`
1. "Vendor" a specific version of the chart into this repo with: `tk tool charts add <repo>/<name>@<version>` , for example: `tk tool charts add elastic/elasticsearch@7.14.0`

### Using the chart in a tanka environment



### Updating a chart ###

1. Update the relevant chart's version in `chartfile.yaml`, under the `requires`
   key.
1. `make vendor`
