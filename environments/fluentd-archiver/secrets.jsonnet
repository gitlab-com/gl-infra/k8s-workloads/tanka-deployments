local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  new(env, namespace, appName):: {
    local labels = {
      'app.kubernetes.io/instance': appName,
      'app.kubernetes.io/name': appName,
    },

    image_pull_secret:
      externalSecrets.externalSecret(
        '%s-image-pull-secret' % appName,
        appName,
        namespace,
        clusters[env].clusterName,
        labels=labels,
        secretType='kubernetes.io/dockerconfigjson',
        secretStorePath='shared',
        secretDataTemplate={
          '.dockerconfigjson': |||
            {"auths": {"registry.ops.gitlab.net": {"auth": "{{ (printf "%s:%s" .username .token) | b64enc }}"}}}
          |||,
        },
        data={
          username: {
            key: 'ops-gitlab-net/registry/gl-infra-registry-ro',
            property: 'username',
          },
          token: {
            key: 'ops-gitlab-net/registry/gl-infra-registry-ro',
            property: 'token',
          },
        },
      ),
  },
}
