local common = import 'gitlab/common.libsonnet';
local redis = import 'redis/redis.libsonnet';
local secrets = import 'secrets.jsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'redis',
  local namespaceName = 'redis',
  local defaultEnvData = {
    namespaceName:: namespaceName,
    createNamespace:: true,
  },
  // @param env Refers to environments in https://about.gitlab.com/handbook/engineering/infrastructure/environments/
  // @param k8sCluster Refers to clusters defined in lib/gitlab/clusters.jsonnet
  // @param clustersEnv Refer to the environments defined in environments/redis/values.jsonnet
  local gkeEnvData(env, k8sCluster=env, clustersEnv=env) = defaultEnvData {
    externalSecretsRole:: appName,
    secretStorePaths:: ['shared'],
    redis: {
      [cluster.name]: redis.new(
        k8sCluster,
        appName,
        cluster,
        nodeSelector={ type: cluster.name },
        tolerations=[
          {
            key: 'gitlab.com/type',
            value: cluster.name,
            effect: 'NoSchedule',
          },
        ],
        zoneAntiAffinity=true
      )
      for cluster in values[clustersEnv].clusters
    },
    redis_secrets: secrets.new(env, namespaceName, appName, values[clustersEnv]),
  },
  envsData:: {
    dev: defaultEnvData {
      redis: {
        [cluster.name]: redis.new('dev', appName, cluster)
        for cluster in values.dev.clusters
      },
    },
    pre: gkeEnvData('pre'),
    gstg: gkeEnvData('gstg'),
    'gstg-gitlab-36dv2': gkeEnvData('gstg', 'gstg-gitlab-36dv2', 'gstg-2'),
    gprd: gkeEnvData('gprd'),
    'gprd-gitlab-3okls': gkeEnvData('gprd', 'gprd-gitlab-3okls', 'gprd-2'),
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}
