local clusters = {
  registryCache: {
    name: 'redis-registry-cache',
    externalMaster: { enabled: false },
    replicaCount: 3,
    redisResourcesMemory: '16Gi',
    specialConfiguration: |||
      save 900 1
      maxmemory 8gb
      maxmemory-policy allkeys-lru
      maxmemory-samples 5
    |||,
    storageClass: 'pd-balanced',
    dataStorageSize: '20Gi',
  },
};

local pubsubClusters = {
  pubsub: {
    name: 'redis-pubsub',
    specialConfiguration: |||
      tcp-backlog 1024
    |||,
    externalMaster: { enabled: false },
    replicaCount: 3,
    redisResourcesMemory: '16Gi',
    storageClass: 'pd-balanced',
    dataStorageSize: '10Gi',
  },
};

{
  pre: {
    clusters: std.objectValues(clusters),
  },
  gstg: {
    local gstgRequestResources = {
      redis: { memory: '20Mi', cpu: '265m' },
      sentinel: { memory: '20Mi', cpu: '127m' },
      exporter: { memory: '20Mi', cpu: '10m' },
      metrics: { memory: '22Mi', cpu: '10m' },
    },
    local gtsgLimitResources = {
      redis: { memory: '16Gi', cpu: 4 },
      sentinel: { memory: '2Gi', cpu: 1 },
      exporter: { memory: '1Gi', cpu: 0.5 },
      metrics: { memory: '1Gi', cpu: 0.5 },
    },
    clusters: std.map(function(cluster) cluster { requests: gstgRequestResources, limits: gtsgLimitResources }, std.objectValues(clusters)),
  },
  'gstg-2': {
    clusters: std.objectValues(pubsubClusters),
  },
  'gprd-2': {
    local pubsubGprdClusters = pubsubClusters {
      pubsub+: {
        specialConfiguration: |||
          maxmemory 8gb
        |||,
      },
    },
    clusters: std.objectValues(pubsubGprdClusters),
  },
  gprd: {
    clusters: std.objectValues(clusters),
  },
  dev: {
    local devLimitResources = {
      redis: { memory: '1Gi', cpu: 0.5 },
      sentinel: { memory: '0.5Gi', cpu: 0.5 },
      exporter: { memory: '0.25Gi', cpu: 0.25 },
      metrics: { memory: '0.25Gi', cpu: 0.25 },
    },
    local devRequestResources = {
      redis: { memory: '50Mi', cpu: '100m' },
      sentinel: { memory: '15Mi', cpu: '10m' },
      exporter: { memory: '30Mi', cpu: '15m' },
      metrics: { memory: '20Mi', cpu: '10m' },
    },
    clusters: std.map(function(cluster) cluster { requests: devRequestResources, limits: devLimitResources, storageClass: '', dataStorageSize: '0.5Gi' }, std.objectValues(clusters)),
  },
}
