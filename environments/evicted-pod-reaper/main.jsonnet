local evictedPodReaper = import 'evicted-pod-reaper/evicted-pod-reaper.libsonnet';
local common = import 'gitlab/common.libsonnet';

{
  local appName = 'evicted-pod-reaper',
  envsData:: {
    gprd: {
      namespaceName:: appName,
      createNamespace:: true,
      cronJob: evictedPodReaper.newCronJob(),
    },
    dev: {
      namespaceName:: appName,
      createNamespace:: true,
      cronJob: evictedPodReaper.newCronJob(),
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData),

}
