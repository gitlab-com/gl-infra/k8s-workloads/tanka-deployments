local common = import 'gitlab/common.libsonnet';
local plantuml = import 'plantuml/plantuml.libsonnet';
local values = import 'values.jsonnet';

{
  local appName = 'plantuml',
  local namespaceName = 'plantuml',

  envsData:: {
    gprd: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      plantuml: plantuml.new(envValues=values.gprd),
    },
    gstg: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      plantuml: plantuml.new(envValues=values.gstg),
    },
    pre: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      plantuml: plantuml.new(envValues=values.pre),
    },
    dev: {
      namespaceName:: namespaceName,
      createNamespace:: false,
      plantuml: plantuml.new(envValues=values.dev),
    },
  },

  envs: common.genGitlabEnvs(
    appName,
    $.envsData,
    {
      labels: {
        'app.kubernetes.io/instance': appName,
        'app.kubernetes.io/name': appName,
        stage: 'main',
        tier: 'sv',
        type: appName,
      },
    },
  ),
}
