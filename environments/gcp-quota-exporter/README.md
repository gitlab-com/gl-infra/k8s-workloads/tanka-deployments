# GCP Quota Exporter

[gcp-quota-exporter Project](https://github.com/mintel/gcp-quota-exporter)
[Reliability Team](https://gitlab.com/gitlab-com/gl-infra)

## Deployment

You can run Tanka on your local dev environment as follows:

```shell
tk diff --with-prune environments/gcp-quota-exporter --name gcp-quota-exporter/dev
```
