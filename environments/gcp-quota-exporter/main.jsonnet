local gcp_quota_exporter = import 'gcp-quota-exporter/gcp-quota-exporter.libsonnet';
local common = import 'gitlab/common.libsonnet';
local values = import 'values.libsonnet';

{
  local appName = 'gcp-quota-exporter',
  local namespaceName = 'gcp-quota-exporter',

  envsData:: {
    gprd: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      newExporter: gcp_quota_exporter.newExporter('gprd', appName, namespaceName, values.gprd),
      newRunnerExporter: gcp_quota_exporter.newExporter('gprd', 'gcp-quota-exporter-runners', namespaceName, values.gprdRunners),
    },
    gstg: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      newExporter: gcp_quota_exporter.newExporter('gstg', appName, namespaceName, values.gstg),
    },
    ops: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      newExporter: gcp_quota_exporter.newExporter('ops', appName, namespaceName, values.ops),
    },
    pre: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      newExporter: gcp_quota_exporter.newExporter('pre', appName, namespaceName, values.pre),
    },
    dev: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      newExporter: gcp_quota_exporter.newExporter('dev', appName, namespaceName, values.dev),
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}
