local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  new(env, namespaceName, appName):: {
    local labels = {
      'app.kubernetes.io/instance': appName,
      'app.kubernetes.io/name': 'woodhouse',
    },
    local clusterName = clusters[env].clusterName,

    local secretKey(name) = externalSecrets.k8sNamespaceSecretKey(clusterName, namespaceName, name),

    appSecrets:
      externalSecrets.externalSecret(
        '%s-v6' % appName,
        appName,
        namespaceName,
        clusterName,
        labels=labels,
        data={
          GITLAB_OPS_API_TOKEN: {
            key: secretKey('gitlab-ops-api'),
            property: 'token',
          },
          GITLAB_API_TOKEN: {
            key: secretKey('gitlab-api'),
            property: 'token',
          },
          [if env == 'ops' then 'GITLAB_OPS_CHECK_TRIGGER_TOKEN']: {
            key: secretKey('gitlab'),
            property: 'ops_check_trigger_token',
            version: 1,
          },
          GITLAB_WEBHOOK_TOKEN: {
            key: secretKey('gitlab'),
            property: 'webhook_token',
            version: 2,
          },
          INCIDENT_CALL_URL: {
            key: secretKey('zoom'),
            property: 'incident_call_url',
            version: 1,
          },
          PAGERDUTY_API_TOKEN: {
            key: secretKey('pagerduty'),
            property: 'api_token',
            version: 1,
          },
          [if env == 'ops' then 'PAGERDUTY_INTEGRATION_KEY_CMOC']: {
            key: secretKey('pagerduty'),
            property: 'integration_key_cmoc',
            version: 1,
          },
          [if env == 'ops' then 'PAGERDUTY_INTEGRATION_KEY_EOC']: {
            key: secretKey('pagerduty'),
            property: 'integration_key_eoc',
            version: 1,
          },
          [if env == 'ops' then 'PAGERDUTY_INTEGRATION_KEY_IMOC']: {
            key: secretKey('pagerduty'),
            property: 'integration_key_imoc',
            version: 1,
          },
          SLACK_BOT_ACCESS_TOKEN: {
            key: secretKey('slack'),
            property: 'bot_access_token',
            version: 1,
          },
          SLACK_SIGNING_SECRET: {
            key: secretKey('slack'),
            property: 'signing_secret',
            version: 1,
          },
          STATUSIO_API_ID: {
            key: secretKey('statusio'),
            property: 'api_id',
            version: 1,
          },
          STATUSIO_API_KEY: {
            key: secretKey('statusio'),
            property: 'api_key',
            version: 1,
          },
        },
      ),
    [if env == 'ops' then 'adminSecrets']:
      externalSecrets.externalSecret(
        '%s-admin-v1' % appName,
        appName,
        namespaceName,
        clusterName,
        labels=labels,
        data={
          SLACK_BOT_ACCESS_TOKEN: {
            key: secretKey('slack_admin'),
            property: 'bot_access_token',
            version: 1,
          },
        },
      ),
  },
}
