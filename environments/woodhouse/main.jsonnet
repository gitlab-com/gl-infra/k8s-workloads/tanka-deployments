local environments = import 'environments.libsonnet';
local common = import 'gitlab/common.libsonnet';
local secrets = import 'secrets.libsonnet';
local woodhouse = import 'woodhouse.libsonnet';

local appName = 'woodhouse';
local namespaceName = 'woodhouse';

function(tag='latest') {
  envsData:: {
    [env]: {
      namespaceName:: namespaceName,
      createNamespace:: true,
      externalSecretsRole:: 'woodhouse',
      woodhouse:
        woodhouse.new(env, namespaceName, appName, tag),
      woodhouseSecrets: secrets.new(env, namespaceName, appName),
    }
    for env in std.objectFields(environments)
  },

  envs: common.genGitlabEnvs(appName, $.envsData),
}
