local podResources = {
  requests: {
    cpu: '1',
    memory: '256Mi',
  },
  limits: {
    memory: '256Mi',
  },
};

{
  ops: {
    secretName: 'woodhouse-v6',
    adminSecretName: 'woodhouse-admin-v1',
    notifyExpiringSilences: {
      enabled: true,
      config: {
        ALERTMANATER_SLACK_CHANNEL: 'C101F3796',  // production
        ALERTMANAGER_BASE_URL: 'http://alertmanager-headless.monitoring.svc.cluster.local:9093',
        ALERTMANAGER_EXTERNAL_BASE_URL: 'https://alerts.gitlab.net',
        SILENCE_END_CUTOFF: '8h',
      },
      // run at 1 minute after each 8-hour SRE shift starts
      // note: this may need to be adjusted on DST change
      // starting with Kubernetes v1.22 it will be possible
      // to specify a timezone in the schedule, see:
      // https://github.com/kubernetes/kubernetes/issues/47202
      schedule: '1 7,15,23 * * *',
      resources: podResources,
    },
    updateOncallUsergroups: {
      eoc: {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'P7IG7DS',
          SLACK_GROUP_ID: 'SRXBY6SSC',
        },
        resources: {
          requests: {
            cpu: '100m',
            memory: '100Mi',
          },
        },
      },
      'incident-managers': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'P3N1TU2',
          SLACK_GROUP_ID: 'S046PL9BCS1',
        },
        resources: {
          requests: {
            cpu: '100m',
            memory: '100Mi',
          },
        },
      },
      cmoc: {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PNH1Z1L',
          SLACK_GROUP_ID: 'S046SGQ3GRH',
        },
        resources: {
          requests: {
            cpu: '100m',
            memory: '100Mi',
          },
        },
      },
      'dedicated-cmoc': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PN032FC',
          SLACK_GROUP_ID: 'S069XU8KYGY',
        },
        resources: {
          requests: {
            cpu: '100m',
            memory: '100Mi',
          },
        },
      },
      ceoc: {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PKV6GCH',
          SLACK_GROUP_ID: 'S05SJ258FJ9',
        },
        resources: podResources,
      },
      'support-manager-oncall': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PGNLUZ1',
          SLACK_GROUP_ID: 'S05SZJT091P',
        },
        resources: podResources,
      },
      'gitaly-oncall': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PZR3CQS',
          SLACK_GROUP_ID: 'S087WNR1Y5P',
        },
        resources: podResources,
      },
      'dbo-oncall': {
        enabled: true,
        config: {
          PAGERDUTY_ESCALATION_POLICY: 'PRGQWRE',
          SLACK_GROUP_ID: 'S08EV96L9BQ',
        },
        resources: podResources,
      },
    },
  },
  gstg: {
  },
  dev: {
    config: {
      GITLAB_INCIDENT_ISSUE_PROJECT_PATH: 'dummy',
      GITLAB_PRODUCTION_PROJECT_PATH: 'dummy',
      INCIDENT_SLACK_CHANNEL: 'dummy',
      LOG_FORMAT: 'logfmt',
    },
    replicas: 1,
    resources: {},
  },
}
