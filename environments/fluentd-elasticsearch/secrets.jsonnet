local clusters = import 'gitlab/clusters.libsonnet';
local externalSecrets = import 'gitlab/external-secrets.libsonnet';

{
  new(env, namespace, appName, labels={}, production_logs=false):: {
    image_pull_secret:
      externalSecrets.externalSecret(
        '%s-image-pull-secret' % appName,
        appName,
        namespace,
        clusters[env].clusterName,
        labels=labels,
        secretLabels=labels,
        secretStorePath='shared',
        secretType='kubernetes.io/dockerconfigjson',
        secretDataTemplate={
          '.dockerconfigjson': |||
            {"auths": {"registry.ops.gitlab.net": {"auth": "{{ (printf "%s:%s" .username .token) | b64enc }}"}}}
          |||,
        },
        data={
          username: {
            key: 'ops-gitlab-net/registry/gl-infra-registry-ro',
            property: 'username',
          },
          token: {
            key: 'ops-gitlab-net/registry/gl-infra-registry-ro',
            property: 'token',
          },
        },
      ),
    output:
      externalSecrets.externalSecret(
        '%s-output' % appName,
        appName,
        namespace,
        clusters[env].clusterName,
        labels=labels,
        secretLabels=labels,
        secretStorePath='shared',
        data={
          local key = 'fluentd/elasticsearch/%s' % (if production_logs then 'prod' else 'non-prod'),
          host: {
            key: key,
            property: 'host',
            version: 1,
          },
          user: {
            key: key,
            property: 'user',
            version: 1,
          },
          password: {
            key: key,
            property: 'password',
            version: 1,
          },
        },
      ),
  },
}
