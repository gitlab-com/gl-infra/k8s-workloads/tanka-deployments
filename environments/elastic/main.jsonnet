local elasticsearch = import 'elastic/elasticsearch/elasticsearch.libsonnet';
local kibana = import 'elastic/kibana/kibana.libsonnet';
local common = import 'gitlab/common.libsonnet';


{
  local appName = 'elastic',
  envsData:: {
    dev: {
      namespaceName:: appName,
      createNamespace:: true,
      elasticsearchCluster: elasticsearch.defaultESCluster(
        conf={
          values: {
            replicas: 1,
            minimumMasterNodes: 1,
            volumeClaimTemplate+: {
              resources+: {
                requests+: {
                  storage: '2Gi',
                },
              },
            },
            resources+: {
              requests+: {
                cpu: 4,
                memory: '4Gi',
              },
              limits+: {
                cpu+: 4,
                memory: '4Gi',
              },
            },
          },
        },
      ),
      kibana: kibana.defaultKibana(),
    },
    gstg: {
      namespaceName:: appName,
      createNamespace:: true,
    },
  },

  envs: common.genGitlabEnvs(appName, $.envsData),

}
