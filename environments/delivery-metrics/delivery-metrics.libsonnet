local environments = import 'environments.libsonnet';
local common = import 'gitlab/common.libsonnet';
local k = import 'ksonnet-util/kausal.libsonnet';

local container = k.core.v1.container;
local port = k.core.v1.containerPort;

local svcPort = 2112;
local probe = {
  httpGet: {
    path: '/metrics',
    port: 'http',
  },
};

{
  new(env, namespace, appName, tag):: {
    namespace: common.namespace(namespace),
    deployment: $.deployment(env, appName, tag),
    service: $.serviceFor(self.deployment),
    serviceMonitor: $.serviceMonitor(env, appName),
  },

  deployment(env, appName, tag)::
    local defaults = {
      config: {},
      image: 'registry.ops.gitlab.net/gitlab-org/release/tools/delivery-metrics',
      replicas: 1,
      resources: {},
      secretName: appName,
      imagePullSecrets: [],
    };
    local envConfig = defaults + environments[env];

    common.deployment(
      name=appName,
      type='delivery-metrics',
      replicas=envConfig.replicas,
      containers=[
        container.new(name='delivery-metrics', image='%s:%s' % [envConfig.image, tag])
        + container.withArgs([
          '-log-format=json',
          '-consul-host=consul-gl-consul-server.consul.svc.cluster.local',
        ])
        + container.withPorts([port.new('http', svcPort)])
        + container.withEnvMap(envConfig.config)
        + {
          // Take imagePullPolicy private to remove it from the final object.
          // We want to rely on the default behaviour of imagePullPolicy to
          // avoid using stale `:latest` images.
          imagePullPolicy:: super.imagePullPolicy,

          envFrom: [
            {
              secretRef: { name: envConfig.secretName },
            },
          ],
          livenessProbe: probe,
          readinessProbe: probe,
          resources: envConfig.resources,
        },
      ],
    ) + {
      metadata+: {
        labels+: {
          name: appName,
        },
      },
      spec+: {
        template+: {
          spec+: (
            if std.objectHas(envConfig, 'nodePool') then
              common.withNodePoolAffinity(envConfig.nodePool)
            else {}
          ) + {
            imagePullSecrets+: [{ name: secret } for secret in envConfig.imagePullSecrets],
          },
        },
      },
    },

  serviceFor(deployment)::
    k.util.serviceFor(deployment) + {
      metadata+: {
        annotations+: {
          'cloud.google.com/neg': '{"ingress": true}',
        },
      },
      spec+: {
        type: 'NodePort',
      },
    },

  serviceMonitor(env, appName):: {
    apiVersion: 'monitoring.coreos.com/v1',
    kind: 'ServiceMonitor',
    metadata: {
      name: appName,
      labels: {
        'app.kubernetes.io/component': 'metrics',
        'app.kubernetes.io/instance': appName,
        'app.kubernetes.io/name': 'delivery-metrics',
        'gitlab.com/prometheus-instance': 'prometheus-system',
      },
    },
    spec: {
      endpoints: [
        {
          interval: '15s',
          path: '/metrics',
          targetPort: svcPort,
          relabelings: [
            {
              targetLabel: 'type',
              replacement: 'delivery-metrics',
            },
            {
              targetLabel: 'tier',
              replacement: 'sv',
            },
            {
              targetLabel: 'stage',
              replacement: 'main',
            },
            {
              targetLabel: 'shard',
              replacement: 'default',
            },
            {
              targetLabel: 'environment',
              replacement: env,
            },
          ],
        },
      ],
      selector: {
        matchLabels: {
          name: appName,
        },
      },
    },
  },

  ingress(env, appName)::
    local envConfig = environments[env];
    {
      ingress: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'Ingress',
        metadata: {
          name: appName,
          annotations: {
            'kubernetes.io/ingress.allow-http': 'false',
            'kubernetes.io/ingress.global-static-ip-name': envConfig.ingress.staticIPName,
            'networking.gke.io/managed-certificates': appName,
          },
        },
        spec: {
          rules: [
            {
              host: envConfig.ingress.domain,
              http: {
                paths: [
                  {
                    backend: {
                      service: {
                        name: appName,
                        port: {
                          number: svcPort,
                        },
                      },
                    },
                    path: '/*',
                    pathType: 'ImplementationSpecific',
                  },
                ],
              },
            },
          ],
        },
      },
      managedCert: common.managedCert(appName, [envConfig.ingress.domain]),
    },
}
